
import { NativeModules } from 'react-native';

const { ReactNativeThreatmetrix } = NativeModules;

export default ReactNativeThreatmetrix;
