
package com.ofx.threatmetrix;

import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.threatmetrix.TrustDefender.Config;
import com.threatmetrix.TrustDefender.EndNotifier;
import com.threatmetrix.TrustDefender.Profile;
import com.threatmetrix.TrustDefender.ProfilingOptions;
import com.threatmetrix.TrustDefender.TrustDefender;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ReactNativeThreatmetrixModule extends ReactContextBaseJavaModule {

    private final static String TAG = ReactNativeThreatmetrixModule.class.getSimpleName();

    private static final String OPTION_ORG_ID = "orgId";
    private static final String OPTION_FP_SERVER = "fingerprintServer";
    private static final String OPTION_LOCATION_SERVICES_WITH_PROMPT = "locationServicesWithPrompt";
    private static final String OPTION_USE_UI_WEBVIEW = "useUIWebView";
    private static final String OPTION_DISABLE_INIT_PACKAGE_SCAN = "disableInitPackageScan";
    private static final String RESULT_SESSION_ID = "sessionId";
    private static final String RESULT_STATUS = "status";
    private static final String RESULT_STATUS_CODE = "statusCode";

    private final ReactApplicationContext reactContext;

    // Options
    private String mOrgId;
    private String mFingerprintServer;
    private Boolean mLocationServices;
    private Boolean mUseUIWebView;
    private Boolean mDisableInitPackageScan;

    // Results
    private String mSessionId;
    private String mStatus;
    private String mStatusCode;
    private WritableMap mResult;

    private Promise mPromise;

    public ReactNativeThreatmetrixModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "ReactNativeThreatmetrix";
    }

    @ReactMethod
    public void init(ReadableMap options, Promise promise) {

        mPromise = promise;

        mOrgId = options.hasKey(OPTION_ORG_ID) ? options.getString(OPTION_ORG_ID) : null;
        mFingerprintServer = options.hasKey(OPTION_FP_SERVER) ? options.getString(OPTION_FP_SERVER) : null;
        mLocationServices = options.hasKey(OPTION_LOCATION_SERVICES_WITH_PROMPT) ? options.getBoolean(OPTION_LOCATION_SERVICES_WITH_PROMPT) : false;
        mUseUIWebView = options.hasKey(OPTION_USE_UI_WEBVIEW) ? options.getBoolean(OPTION_USE_UI_WEBVIEW) : false;
        mDisableInitPackageScan = options.hasKey(OPTION_DISABLE_INIT_PACKAGE_SCAN) ? options.getBoolean(OPTION_DISABLE_INIT_PACKAGE_SCAN) : true;

        // Creating a config instance to be passed to the init() method.
        Config config = new Config()
                .setOrgId(mOrgId)
                .setFPServer(mFingerprintServer)
                .setContext(this.reactContext)
                .setTimeout(10, TimeUnit.SECONDS)
                .setDisableWebView(mUseUIWebView)
                .setRegisterForLocationServices(mLocationServices)
                .setDisableInitPackageScan(mDisableInitPackageScan);

        try {
            TrustDefender.getInstance().init(config);
            Log.d(TAG, "Threatmetrix successfully initalized");
            WritableMap map = Arguments.createMap();
            mPromise.resolve(map);
        } catch (IllegalArgumentException exception) {
            Log.e(TAG, "Threatmetrix initialize was not successful " + exception.toString() + ". Can't perform profiling.");
            mPromise.resolve(null);
        }
    }

    @ReactMethod
    public void doProfile(String sessionId, ReadableArray customAttributes, Promise promise) {
        // Reset state of properties to ensure we have a clean activity
        reset();
        mPromise = promise;

        try {
            profile(sessionId, customAttributes);
        } catch (IllegalArgumentException exception) {
            Log.e(TAG, "Threatmetrix profiling was not successful: " + exception.toString());
            mPromise.resolve(null);
        }
    }

    void profile(String sessionId, ReadableArray customAttributes) {
        Log.d(TAG, "Threatmetrix profiling begin");

        // Optional additional attribute configuration to use during profiling
        List<String> attributes = new ArrayList<String>();
        for (int i = 0; i < customAttributes.size(); i++) {
            attributes.add(i, customAttributes.getString(i));
        }
        ProfilingOptions options = new ProfilingOptions()
                .setSessionID(sessionId)
                .setCustomAttributes(attributes);

        // Fire off the profiling request
        // The end notifier must be passed to the doProfileRequest() method
        Profile.Handle profilingHandle = TrustDefender.getInstance().doProfileRequest(options, new CompletionNotifier());

        Log.d(TAG, "Threatmetrix result session id = " + profilingHandle.getSessionID());
    }

    void reset() {
        mSessionId = "";
        mStatus = "";
        mStatusCode = "";
        mPromise = null;
        mResult = Arguments.createMap();
    }

    /**
     * Used for notification from the SDK.
     */
    private class CompletionNotifier implements EndNotifier {

        // This gets called when the profiling has finished.
        @Override
        public void complete(Profile.Result result) {
            // Save the results in preparation to resolve promise
            mSessionId = result.getSessionID();
            mStatusCode = result.getStatus().toString();
            mStatus = result.getStatus().getDesc();

            mResult.putString(RESULT_SESSION_ID, mSessionId);
            mResult.putString(RESULT_STATUS, mStatus);
            mResult.putString(RESULT_STATUS_CODE, mStatusCode);

            Log.i(TAG, "Threatmetrix profile completed with: " + result.getStatus().toString() + " - " + result.getStatus().getDesc());

            //Profiling is complete, so login can proceed when the Login button is clicked.
            mPromise.resolve(mResult);
        }
    }

}