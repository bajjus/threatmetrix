//
//  ReactNativeThreatmetrix.h
//  ReactNativeThreatmetrix
//
//  Created by Liam Andrew on 23/8/18.
//  Copyright © 2018 OFX. All rights reserved.
//

#if __has_include("RCTBridgeModule.h")
#import "RCTBridgeModule.h"
#else
#import <React/RCTBridgeModule.h>
#endif

#import "ProfileController.h"

@interface ReactNativeThreatmetrix : NSObject <RCTBridgeModule>

@property ProfileController *td;

@end
  
