//
//  ProfileController.h
//  ReactNativeThreatmetrix
//
//  Created by Liam Andrew on 23/8/18.
//  Copyright © 2018 OFX. All rights reserved.
//

@import Foundation;
@import TrustDefender;

@interface ProfileController : NSObject

// The TrustDefender SDK instance
@property THMTrustDefender* profile;

// Has the profile completed
@property BOOL profileIsFinished;
// Timeout for profiling
@property NSNumber *timeout;

// Threatmetrix connection parameters
@property NSString *orgID;
@property NSString *fingerPrintServer;
@property NSNumber *locationServicesWithPrompt;
@property NSNumber *useUIWebView;

// Threatmetrix result parameters
@property NSString *sessionId;
@property NSString *status;
@property NSString *statusCode;

// Instantiate an instance of TrustDefenderSDK and perform the initial configuration setup
- (instancetype) initWithOptions:(NSDictionary *)options;

// Perform profiling on the device
- (void) doProfileForSession:(NSString *)sessionId withCustomAttributes:(NSArray *)customAttributes;

// Reset the profiling state on the device
- (void) reset;

@end
