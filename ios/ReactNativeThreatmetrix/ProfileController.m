//
//  ProfileController.m
//  ReactNativeThreatmetrix
//
//  Created by Liam Andrew on 23/8/18.
//  Copyright © 2018 OFX. All rights reserved.
//

#import "ProfileController.h"

NSString* const kOptionOrgIdKey = @"orgId";
NSString* const kOptionFingerprintServerKey = @"fingerprintServer";
NSString* const kOptionLocationServicesWithPrompt = @"locationServicesWithPrompt";
NSString* const kOptionUseUIWebView = @"useUIWebView";

@implementation ProfileController

- (instancetype) initWithOptions:(NSDictionary *)options {
    self = [super init];
    static dispatch_once_t configureOnce = 0;
    
    NSLog(@"Initialising Threatmetrix with options:");
    for(NSString *key in [options allKeys]) {
        NSLog(@"Key: %@, value: %@", key, [options objectForKey:key]);
    }
    
    [self reset];
    self.timeout = @10;
    self.profile = [THMTrustDefender sharedInstance];
    
    // Threatmetrix configuration options
    self.orgID = [options valueForKey:kOptionOrgIdKey];
    self.fingerPrintServer = [options valueForKey:kOptionFingerprintServerKey];
    self.locationServicesWithPrompt = [options valueForKey:kOptionLocationServicesWithPrompt];
    if (!self.locationServicesWithPrompt) {
        self.locationServicesWithPrompt = @NO;
    }
    self.useUIWebView = [options valueForKey:kOptionUseUIWebView];
    if (!self.useUIWebView) {
        self.useUIWebView = @NO;
    }
    
    // Configure is effective only once (subsequent calls are ignored) so ensure that it is only dispatched once
    dispatch_once(&configureOnce,
                  ^{
                      [self.profile configure:@{
                                                THMOrgID: self.orgID,
                                                THMFingerprintServer: self.fingerPrintServer,
                                                THMTimeout: self.timeout,
                                                THMLocationServicesWithPrompt: self.locationServicesWithPrompt,
                                                THMUseUIWebView: self.useUIWebView,
                                                }];
                  });
    
    NSLog(@"Threatmetrix initialised successfully.");
    return self;
}

- (void) doProfileForSession:(NSString *)sessionId withCustomAttributes:(NSArray *)customAttributes {
    NSLog(@"Begin Threatmetrix profile with sessionId: %@", sessionId);
    self.profileIsFinished = NO;
    
    NSMutableDictionary *options = [[NSMutableDictionary alloc] init];
    [options setObject:customAttributes forKey:THMCustomAttributes];
  
    if (sessionId) {
        [options setObject:sessionId forKey:THMSessionID];
    }
    
    // Fire off the profiling request.
    THMProfileHandle *profileHandle = [self.profile
                                       doProfileRequestWithOptions:options
                                       andCallbackBlock:^(NSDictionary *result)
                                       {
                                           THMStatusCode statusCode = [[result valueForKey:THMProfileStatus] integerValue];
                                           self.statusCode = [@(statusCode) stringValue];
                                           
                                           // Store the status of the profiling request
                                           switch (statusCode) {
                                               case THMStatusCodeOk:
                                                   self.status = @"OK";
                                                   break;
                                               case THMStatusCodeNetworkTimeoutError:
                                                   self.status = @"Timed out";
                                                   break;
                                               case THMStatusCodeConnectionError:
                                                   self.status = @"Connection Error";
                                                   break;
                                               case THMStatusCodeHostNotFoundError:
                                                   self.status = @"Host not found Error";
                                                   break;
                                               case THMStatusCodeInternalError:
                                                   self.status = @"Internal Error";
                                                   break;
                                               case THMStatusCodeInterruptedError:
                                                   self.status = @"Interrupted";
                                                   break;
                                               default:
                                                   self.status = @"Other";
                                                   break;
                                           }
                                           
                                           NSLog(@"Profile completed with: %@ and session ID: %@", self.status, [result valueForKey:THMSessionID]);
                                           [self setProfileIsFinished:YES];
                                       }];
    
    // Collect the session Id
    self.sessionId = profileHandle.sessionID;
    NSLog(@"Session id is %@", self.sessionId);
}

- (void) reset {
    self.sessionId = nil;
    self.status = nil;
    self.statusCode = nil;
    self.profileIsFinished = nil;
}

@end

