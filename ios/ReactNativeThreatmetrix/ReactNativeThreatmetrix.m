//
//  ReactNativeThreatmetrix.m
//  ReactNativeThreatmetrix
//
//  Created by Liam Andrew on 23/8/18.
//  Copyright © 2018 OFX. All rights reserved.
//

#import "ReactNativeThreatmetrix.h"
#import <React/RCTConvert.h>
#if __has_include("RCTUtils.h")
#import "RCTUtils.h"
#else
#import <React/RCTUtils.h>
#endif

static NSString* const kSessionId = @"sessionId";
static NSString* const kStatus = @"status";
static NSString* const kStatusCode = @"statusCode";

@implementation ReactNativeThreatmetrix

RCT_EXPORT_MODULE();

RCT_REMAP_METHOD(init,
                 options:(NSDictionary *)options
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
    
    self.td = [[ProfileController alloc] initWithOptions:options];
    NSDictionary *result = @{
                             kStatus: @"Success",
                             };
    resolve(result);
}

RCT_REMAP_METHOD(doProfile,
                 sessionId: (NSString *)sessionId
                 customAttributes:(NSArray *)customAttributes
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
    
    
    // Begin profiling with any custom attributes
    [self.td doProfileForSession:sessionId withCustomAttributes:customAttributes];
    
    // Ensure that profiling is complete before we resolve
    while([self.td profileIsFinished] == NO)
    {
        // If profiling isn't complete, wait before submitting
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeInterval:[[self.td timeout] intValue] sinceDate:[NSDate date]]];
    }
    NSLog(@"Session id is %@", self.td.sessionId);
    NSDictionary *result = @{
                             kSessionId: self.td.sessionId,
                             kStatus: self.td.status,
                             kStatusCode: self.td.statusCode,
                             };
    [self.td reset];
    resolve(result);
}
    
@end
  
