
# react-native-threatmetrix

## Getting started

`$ npm install react-native-threatmetrix --save`

### Mostly automatic installation

`$ react-native link react-native-threatmetrix`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-threatmetrix` and add `ReactNativeThreatmetrix.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libReactNativeThreatmetrix.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Add a direct path to the the TrustDefender.framework in your project's Framework search paths:
	```
	$(SRCROOT)/../../react-native-threatmetrix/ios
	```
5. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.ReactNativeThreatmetrixPackage;` to the imports at the top of the file
  - Add `new ReactNativeThreatmetrixPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
	include ':@ofx:TrustDefender-5.2-36'
	project(':@ofx:TrustDefender-5.2-36').projectDir = new File(rootProject.projectDir,'../node_modules/@ofx/react-native-threatmetrix/android/TrustDefender-5.2-36')
  	include ':react-native-threatmetrix'
  	project(':react-native-threatmetrix').projectDir = new File(rootProject.projectDir, 	'../node_modules/@ofx/react-native-threatmetrix/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':@ofx:react-native-threatmetrix')
  	```

## TROUBLESHOOTING

* Ensure that "Enable modules (C and Objective-C)" options in the project setting is set to "YES".
* Remember to add -ObjC linker flag in Xcode
* Add a direct path to the the TrustDefender.framework in your project's Framework search paths:
	```
	$(SRCROOT)/../../react-native-threatmetrix/ios
	```

## Usage
```javascript
import ReactNativeThreatmetrix from '@ofx/react-native-threatmetrix';
```
Initialise Threatmetrix instance
* **threatmetrixOptions**: *Object* - described below
```javascript
ReactNativeThreatmetrix.init(threatmetrixOptions)
```


Perform Threatmetrix profiling
* **sessionId**: *String* | *null*: if you have an ID already calculated then you can pass it in. Otherwise pass null for Threatmetrix to generate a new one.
* **customAttributes**: *Array* - described below
```javascript
// Perform profiling
ReactNativeThreatmetrix.doProfile(sessionId, customAttributes);
```
## Options:

```
options = {
	orgId: 'enter-org-id',
        fingerprintServer: 'enter-fp-server',
	locationServicesWithPrompt: false,
	useUIWebView: false,
	disableInitPackageScan: true,
};
```

## Options Descriptions:

**orgId**
**Mandatory.** Specifies the Org ID.

**fingerprintServer**
**Mandatory.** The fully qualitfied domain name of the server that ThreatMetrix SDK will communicate with in order to transmit the collected device attributes.

**locationServicesWithPrompt**
**Optional** - *Boolean* Default is false.

**useUIWebView**
**Optional** - *Boolean* Default is false.

**disableInitPackageScan**
**Optional** - *Boolean* Android specific value to drive whether to do a package scan on device during init. Default is true.

## Custom Attributes

An array of up to 5 custom attributes:
```
customAttributes = ["attribute 1", "attribute 2"];
  